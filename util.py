class Debug:
    def __init__(self, level: int = 0):
        self.level = level

    def __call__(self, *args, level=0, sep='\n', **kwargs):
        if level >= self.level:
            print(*args, **kwargs)

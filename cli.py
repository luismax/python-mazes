import sys
from os import mkdir
from os.path import isdir

from PIL import Image

from coord import Coord
from generator import generate
from maze import Maze
from solver import solve
from util import Debug

debug = Debug(level=3)


def __main__():
    size = int(sys.argv[1]) if len(sys.argv) == 2 else 15
    m = Maze(size, size)
    generate(m, debug)
    debug('generated a {}x{} maze with {:.3%} ocupation, '.format(size, size, m.occupation), end='', level=3)
    debug('it is ', level=3, end='')
    path = solve(m, debug)
    if path:
        debug('solvable; this one possible solution:', level=3)
    else:
        debug('NOT solvable; best luck next time', level=3)
        exit(0)

    save_image(m, Coord(size, size), 'unsolved')
    for coord in path:
        m._mark_as_('~', coord)
    debug(path, level=3)
    save_image(m, Coord(size, size), 'solved')


color_map = {
    '@': (255, 0, 0),
    '#': (0, 255, 0),
    '·': (255, 0, 255),
    'x': (0, 0, 0),
    None: (0, 0, 0),
    '~': (0, 255, 255),
}


def save_image(maze: Maze, size: Coord, kind) -> None:
    multiplier = 10
    image = Image.new('RGB', size * multiplier)
    pixels = image.load()
    for ((x, y), pixel) in maze.values:
        for x_delta in range(multiplier):
            for y_delta in range(multiplier):
                if color_map.get(pixel, False):
                    pixels[x * multiplier + x_delta, y * multiplier + y_delta] = color_map[pixel]
    if not isdir('output'):
        mkdir('output')
    image.save('output/maze_{x}x{y}_{kind}.png'.format(**size._asdict(), kind=kind))


if __name__ == '__main__':
    __main__()

from typing import NamedTuple


class Coord(NamedTuple):
    x: int
    y: int

    def __mul__(self, other):
        if isinstance(other, int):
            return self.__class__(self.x * other, self.y * other)

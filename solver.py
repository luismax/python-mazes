from collections import deque
from typing import Deque, Tuple, Optional, Set

from coord import Coord
from maze import Maze
from util import Debug


def solve(maze: Maze, debug: Debug) -> Optional[Tuple[Coord, ...]]:
    q: Deque[Tuple] = deque()
    visited: Set[Coord] = set()

    path: Tuple = (maze.start,)
    q.append(path)
    visited.add(maze.start)

    while q:
        path: Tuple = q.pop()
        debug(path)

        last = path[-1]
        debug(last)

        if maze.is_end(last):
            return path

        neighbors = maze.neighbors(last, ('path', 'end'))

        for element in neighbors:
            if element not in path and element not in visited:
                next_candidate = path + (element,)
                q.append(next_candidate)
                visited.add(element)
                # print(next_candidate, maze, sep='\n', file=sys.stderr, end='\n' * 2)

        debug('q: {}, path: {}'.format(len(q), len(path)), level=1)
    return None

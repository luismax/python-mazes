from functools import partial
from typing import Callable, List, Optional

from coord import Coord


class Maze:
    is_start: Callable[[Coord], bool]
    is_wall: Callable[[Coord], bool]
    is_end: Callable[[Coord], bool]
    is_path: Callable[[Coord], bool]
    is_empty: Callable[[Coord], bool]

    mark_as_start: Callable[[Coord], None]
    mark_as_wall: Callable[[Coord], None]
    mark_as_end: Callable[[Coord], None]
    mark_as_path: Callable[[Coord], None]

    KINDS = {
        'start': '@',
        'end': '#',
        'wall': 'x',
        'path': '·',
        'empty': None
    }

    def __init__(self, width: int, height: int, start: Optional[tuple] = None, end: Optional[tuple] = None):
        self.width = width
        self.height = height
        self._start: Coord = start if start else (0, 0)
        self._end: Coord = end if end else (height - 1, width - 1)
        self._cells: List[List[str]] = [[None for _ in range(width)] for _ in range(height)]

        for name, func in [(kind, partial(self._is_, char)) for kind, char in self.KINDS.items()]:
            setattr(self, 'is_{}'.format(name), func)

        for name, func in [(kind, partial(self._mark_as_, char)) for kind, char in self.KINDS.items()]:
            setattr(self, 'mark_as_{}'.format(name), func)

        self.mark_as_start(self._start)
        self.mark_as_end(self._end)

    def __str__(self):
        return '\n'.join(map(lambda row: ' '.join(['{0:^5}'.format(x if x else '~') for x in row]), self._cells))

    @property
    def start(self) -> Coord:
        return Coord(*self._start)

    @property
    def occupation(self) -> float:
        return 1 - (sum(map(lambda row: row.count(None), self._cells)) / (self.width * self.height))

    @property
    def values(self):
        # return ([((row, col), self._cells[row][col]) for col in range(self.width)] for row in range(self.height))
        for row in range(self.height):
            for col in range(self.width):
                yield ((row, col), self._cells[row][col])

    def _is_(self, what, coord: Coord) -> bool:
        row, col = coord
        self._check_bounds(row, col)
        return self._cells[row][col] == what

    def _mark_as_(self, kind: str, coord: Coord):
        row, col = coord
        self._check_bounds(row, col)
        self._cells[row][col] = kind

    def empty_neighbors(self, coord: Coord) -> List[Coord]:
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        candidates = (Coord(delta.x + coord.x, delta.y + coord.y) for delta in map(lambda x: Coord(*x), directions))

        return [candidate for candidate in candidates if
                0 <= candidate.y < self.height and 0 <= candidate.x < self.width
                and self.is_empty(candidate)]

    def neighbors(self, coord: Coord, types: tuple = ()) -> List[Coord]:
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        candidates = (Coord(delta.x + coord.x, delta.y + coord.y) for delta in map(lambda x: Coord(*x), directions))
        kinds = [self.KINDS[x] for x in types]

        return [candidate for candidate in candidates if
                0 <= candidate.y < self.height and 0 <= candidate.x < self.width
                and (self._cells[candidate.x][candidate.y] in kinds)]

    def _check_bounds(self, row, col) -> None:
        if not 0 <= row < self.height or not 0 <= col < self.width:
            raise IndexError('Out of index: ({},{}) for ({}x{}) sudoku'.format(row, col, self.width, self.height))

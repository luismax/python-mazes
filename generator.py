from collections import deque
from math import ceil
from random import shuffle
from typing import Deque, Tuple, Set

from coord import Coord
from maze import Maze
from util import Debug


def generate(maze: Maze, debug: Debug) -> Maze:
    q: Deque[Tuple] = deque()
    visited: Set[Coord] = set()

    path: Tuple = (maze.start,)
    q.append(path)
    visited.add(maze.start)

    while q:
        path: Tuple = q.pop()
        debug(path)

        last = path[-1]
        debug(last)

        neighbors = maze.neighbors(last, ('empty',))
        shuffle(neighbors)
        f = ceil(len(neighbors) / 2)
        pathable = neighbors[:f]
        wallable = neighbors[f:]

        for element in wallable:
            maze.mark_as_wall(element)

        for element in pathable:
            if element not in path and element not in visited:
                maze.mark_as_path(element)
                next_candidate = path + (element,)
                q.append(next_candidate)
                visited.add(element)
                # print(next_candidate, maze, sep='\n', file=sys.stderr, end='\n' * 2)

        debug('q: {}, path: {}'.format(len(q), len(path)), level=1)
    return maze
